import { parallelLimit, AsyncFunction, Dictionary } from 'async'

new Promise(async () => {
  const tasks = Array(10)
    .fill(null)
    .map((item, i) => async () => i)
  const limit = 5
  const promise = parallelLimit<any, number[], any>(tasks, limit)
  console.log('Метка №1')
  const results = await promise
      .catch(() => console.log('catch'))
      .finally(() => console.log('finally'))) || []
  console.log('Метка №2')
  console.log(results.join(','))
})
