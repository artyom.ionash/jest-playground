import TaskManager, { parallelLimit } from '../lib/task-manager'

const createSequence = (size: number) =>
  Array(size)
    .fill(null)
    .map((item, i) => i)

describe('Тестирование работы Исполнителей', () => {
  it('Группа исполнителей', async () => {
    const tasks = createSequence(10).map((s) => async () => s)
    const results = await TaskManager.parallelLimit(tasks, 3)
    expect(results).toEqual(createSequence(10))
  })

  it('Многоразовое использование', async () => {
    const tasks1 = createSequence(15).map((s) => async () => s)
    const tasks2 = createSequence(10).map((s) => async () => s)
    await parallelLimit(tasks1, 3)
    const results = await TaskManager.parallelLimit(tasks2, 3)
    expect(results).toEqual(createSequence(10))
  })
})
