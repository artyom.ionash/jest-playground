const samples = Array(10)
  .fill(null)
  .map((item, i) => i)

describe('async / await', () => {
  it('Массив Обещаний', async () => {
    const promises = samples.map((s) => new Promise((resolve) => resolve(s)))
    const five = await promises[5]
    expect(five).toEqual(5)
  })

  it('Экранирование Обещаний стрелочными функциями', async () => {
    const tasks = samples.map((s) => async () => s)
    const five = await tasks[5]()
    expect(five).toEqual(5)
  })
})
