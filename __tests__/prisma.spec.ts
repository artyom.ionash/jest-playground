import prisma from '../lib/prisma'

const samples = [
  { email: 'alice@prisma.io', id: 1, name: 'Alice' },
  { email: 'nilu@prisma.io', id: 2, name: 'Nilu' },
  { email: 'mahmoud@prisma.io', id: 3, name: 'Mahmoud' },
]

describe('prisma', () => {
  it('Получение списка пользователей', async () => {
    const users = await prisma.user.findMany()
    samples.forEach((sample) => expect(users).toContainEqual(sample))
  })
})
