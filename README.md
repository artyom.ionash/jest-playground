# Игровая площадка Jest

Проект для тестирования возможностей Node.js приложений.

## Начало работы с Node.js

```bash
npm init -y
npm install typescript ts-node @types/node --save-dev
```

## Интегрируйте с Jest.js

```bash
npm install --save-dev jest typescript ts-jest @types/jest
npx ts-jest config:init
```

Jest.js - моя любимая система тестирования.

- [ ] [Начало работы](https://kulshekhar.github.io/ts-jest/docs/getting-started/installation)

## Интегрируйте с ORM Prisma

ORM Prisma - моя любимая ORM.

- [ ] [Начало работы](https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch/relational-databases-typescript-postgres)
- [ ] [Первая миграция](https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch/relational-databases/using-prisma-migrate-typescript-postgres)
- [ ] [Семя](https://www.prisma.io/docs/guides/database/seed-database)
- [ ] [Шаблон семени](https://github.com/prisma/prisma-examples/blob/latest/typescript/rest-nextjs-express/backend/prisma/seed.ts)
- [ ] [Клиент](https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch/relational-databases/install-prisma-client-typescript-postgres)

```bash
npm init -y
npm install prisma typescript ts-node @types/node --save-dev
npm install @prisma/client
npx prisma migrate dev --name init
npx prisma migrate reset
npx ts-node index.ts
```

## Интегрируйте с Prettier

Prettier - моё любимое форматирование.

```bash
npm install --save-dev --save-exact prettier
npx prettier --write .

```

- [ ] [Начало работы](https://prettier.io/docs/en/install.html)

```js
// .prettierrc.js
module.exports = {
  semi: false,
  trailingComma: 'es5',
  singleQuote: true,
  printWidth: 80,
  tabWidth: 2,
  useTabs: false,
}
```

```
# .prettierignore
# Ignore artifacts:
build
coverage
```

## Использование GraphQL API

Вы также можете получить прямой доступ к GraphQL API сервера API. Он работает на том же хост-компьютере и на том же порту, и к нему можно получить доступ через маршрут `/api` (в данном случае это [`localhost:3000/api`](http://localhost:3000/api)).

Ниже приведен ряд операций, которые вы можете отправить по API.

### Пример: получить все опубликованные сообщения и их авторов

```graphql
query {
  feed {
    id
    title
    content
    published
    author {
      id
      name
      email
    }
  }
}
```

<Details><Summary><strong>Посмотреть больше операций API</strong></Summary>

### Создать нового пользователя

```graphql
mutation {
  signupUser(name: "Sarah", email: "sarah@prisma.io") {
    id
  }
}
```

### Создать новый черновик

```graphql
mutation {
  createDraft(
    title: "Join the Prisma Slack"
    content: "https://slack.prisma.io"
    authorEmail: "alice@prisma.io"
  ) {
    id
    published
  }
}
```

### Опубликовать существующий черновик

```graphql
mutation {
  publish(postId: "__POST_ID__") {
    id
    published
  }
}
```

> **Note**: You need to replace the `__POST_ID__`-placeholder with an actual `id` from a `Post` item. You can find one e.g. using the `filterPosts`-query.

### Поиск сообщений с определенным заголовком или содержанием

```graphql
{
  filterPosts(searchString: "graphql") {
    id
    title
    content
    published
    author {
      id
      name
      email
    }
  }
}
```

### Получить одно сообщение

```graphql
{
  post(postId: "__POST_ID__") {
    id
    title
    content
    published
    author {
      id
      name
      email
    }
  }
}
```

> **Note**: You need to replace the `__POST_ID__`-placeholder with an actual `id` from a `Post` item. You can find one e.g. using the `filterPosts`-query.

### Удалить сообщение

```graphql
mutation {
  deletePost(postId: "__POST_ID__") {
    id
  }
}
```

> **Note**: You need to replace the `__POST_ID__`-placeholder with an actual `id` from a `Post` item. You can find one e.g. using the `filterPosts`-query.

</Details>

## Развитие приложения

Развитие приложения обычно требует трех шагов:

1. Напишите миграцию базы данных с помощью Prisma Migrate
1. Обновите код приложения на стороне сервера
1. Создавайте новые функции пользовательского интерфейса в React

В следующем примере сценария предположим, что вы хотите добавить в приложение функцию «профиль», где пользователи могут создать профиль и написать краткую биографию о себе.

### 1. Напишите миграцию базы данных с помощью Prisma Migrate

Первым шагом является добавление новой таблицы, например под названием `Profile`, в базу данных. Вы можете сделать это, добавив новую модель в [файл схемы Prisma](./prisma/schema.prisma), а затем запустив миграцию:

```diff
// ./prisma/schema.prisma

model User {
  id      Int      @default(autoincrement()) @id
  name    String?
  email   String   @unique
  posts   Post[]
+ profile Profile?
}

model Post {
  id        Int      @id @default(autoincrement())
  title     String
  content   String?
  published Boolean  @default(false)
  author    User?    @relation(fields: [authorId], references: [id])
  authorId  Int?
}

+model Profile {
+  id     Int     @default(autoincrement()) @id
+  bio    String?
+  user   User    @relation(fields: [userId], references: [id])
+  userId Int     @unique
+}
```

После того, как вы обновите свою модель данных, вы можете выполнить изменения в своей базе данных с помощью следующей команды:

```
npx prisma migrate dev --name add-profile
```

Это добавит еще одну миграцию в каталог `prisma/migrations` и создаст новую таблицу `Profile` в базе данных.

### 2. Обновите код приложения

Теперь вы можете использовать свой экземпляр `PrismaClient` для выполнения операций с новой таблицей `Profile`. Эти операции можно использовать для реализации запросов и изменений (mutations) в GraphQL API.

#### 2.1. Добавьте тип `Profile` в вашу схему GraphQL.

Во-первых, добавьте новый тип GraphQL с помощью функции Nexus `objectType`:

```bash
# https://nexusjs.org/docs/getting-started/tutorial/chapter-setup-and-first-query#cli
npm install nexus graphql apollo-server
npm install --save-dev typescript ts-node-dev
```

##### tsconfig.json

```json
{
  "compilerOptions": {
    "target": "ES2018",
    "module": "commonjs",
    "lib": ["esnext"],
    "strict": true,
    "rootDir": ".",
    "outDir": "dist",
    "sourceMap": true,
    "esModuleInterop": true
  }
}
```

##### package.json

```json
"scripts": {
  "dev": "ts-node-dev --transpile-only --no-notify api/index.ts",
  "build": "tsc"
}
```

```diff
// ./pages/api/index.ts

+const Profile = objectType({
+  name: 'Profile',
+  definition(t) {
+    t.nonNull.int('id')
+    t.string('bio')
+    t.field('user', {
+      type: 'User',
+      resolve: (parent) => {
+        return prisma.profile
+          .findUnique({
+            where: { id: parent.id || undefined },
+          })
+          .user()
+      },
+    })
+  },
+})

const User = objectType({
  name: 'User',
  definition(t) {
    t.nonNull.int('id')
    t.string('name')
    t.nonNull.string('email')
    t.nonNull.list.nonNull.field('posts', {
      type: 'Post',
      resolve: (parent) => {
        return prisma.user
          .findUnique({
            where: { id: parent.id || undefined },
          })
          .posts()
      },
    })
+   t.field('profile', {
+     type: 'Profile',
+     resolve: (parent) => {
+       return prisma.user.findUnique({
+         where: { id: parent.id }
+       }).profile()
+     }
+   })
  },
})
```

Не забудьте включить новый тип в массив `types`, который передается в `makeSchema`:

```diff
export const schema = makeSchema({
  types: [
    Query,
    Mutation,
    Post,
    User,
+   Profile,
    GQLDate
  ],
  // ... as before
}
```

Обратите внимание, что для устранения любых ошибок типов ваш сервер разработки должен быть запущен, чтобы можно было сгенерировать типы Nexus. Если он не запущен, вы можете запустить его с помощью `npm run dev`.

#### 2.2. Добавьте мутацию `createProfile` GraphQL

```diff
// ./pages/api/index.ts

const Mutation = objectType({
  name: 'Mutation',
  definition(t) {

    // other mutations

+   t.field('addProfileForUser', {
+     type: 'Profile',
+     args: {
+       email: stringArg(),
+       bio: stringArg()
+     },
+     resolve: async (_, args) => {
+       return prisma.profile.create({
+         data: {
+           bio: args.bio,
+           user: {
+             connect: {
+               email: args.email || undefined,
+             }
+           }
+         }
+       })
+     }
+   })
  }
})
```

Наконец, вы можете протестировать новую мутацию следующим образом:

```graphql
mutation {
  addProfileForUser(email: "mahmoud@prisma.io", bio: "I like turtles") {
    id
    bio
    user {
      id
      name
    }
  }
}
```

<details><summary>Разверните, чтобы просмотреть другие примеры запросов Prisma Client с <code>Profile</code></summary>

Вот еще несколько примеров клиентских запросов Prisma для нового <code>Profile</code> model:

##### Создать новый профиль для существующего пользователя

```ts
const profile = await prisma.profile.create({
  data: {
    bio: 'Hello World',
    user: {
      connect: { email: 'alice@prisma.io' },
    },
  },
})
```

##### Создайте нового пользователя с новым профилем

```ts
const user = await prisma.user.create({
  data: {
    email: 'john@prisma.io',
    name: 'John',
    profile: {
      create: {
        bio: 'Hello World',
      },
    },
  },
})
```

##### Обновить профиль существующего пользователя

```ts
const userWithUpdatedProfile = await prisma.user.update({
  where: { email: 'alice@prisma.io' },
  data: {
    profile: {
      update: {
        bio: 'Hello Friends',
      },
    },
  },
})
```

</details>

### 3. Создавайте новый функционал пользовательского интерфейса в React

После того, как вы добавили новый запрос или мутацию в API, вы можете приступить к созданию нового компонента пользовательского интерфейса в React. Он мог бы называться, например, `profile.tsx`, находиться в каталоге `pages`.

In the application code, you can access the new operations via Apollo Client and populate the UI with the data you receive from the API calls.
В коде приложения вы можете получить доступ к новым операциям через Apollo Client и заполнить пользовательский интерфейс данными, которые вы получаете от вызовов API.

## Следующие шаги

- Почитайте [Prisma docs](https://www.prisma.io/docs)
- Поделитесь отзывами на канале [`prisma2`](https://prisma.slack.com/messages/CKQTGR6T0/) на [Prisma Slack](https://slack.prisma.io/)
- Западайте вопросы и запросы на решение проблемы на [GitHub](https://github.com/prisma/prisma/)
