import { PrismaClient } from '@prisma/client'

// Источник решения: https://github.com/prisma/prisma-examples/blob/latest/typescript/graphql-nextjs/lib/prisma.ts

// PrismaClient is attached to the `global` object in development to prevent
// exhausting your database connection limit.
//
// Learn more:
// https://pris.ly/d/help/next-js-best-practices

let prisma: PrismaClient

// if (process.env.NODE_ENV === "production") {
//   prisma = new PrismaClient();
// } else {
//   if (!global.prisma) {
//     global.prisma = new PrismaClient();
//   }
//   prisma = global.prisma;
// } // для этого нужен "strict": true в tsconfig.json
prisma = new PrismaClient()
export default prisma
