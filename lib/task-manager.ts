interface Task<R> {
  (): Promise<R>
}

async function parallelLimit<R>(tasks: Task<R>[], limit: number): Promise<R[]> {
  if (limit < 1) throw new Error('[TaskManager] Размер очереди меньше единицы.')

  const results: R[] = Array(tasks.length)
  const executors = Array(limit)
    .fill(null)
    .map(() => async (taskPool: Task<R>[]) => {
      while (true) {
        const task = taskPool.pop()
        if (!task) break
        const index = taskPool.length
        results[index] = await task()
      }
    })
  const taskPool = [...tasks]
  await Promise.all(executors.map((executor) => executor(taskPool)))
  return results
}

export default { parallelLimit }

export { parallelLimit }
